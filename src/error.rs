use std::any::Any;
use thiserror::Error;

pub type Result<T, E = JoinError> = std::result::Result<T, E>;

#[derive(Debug, Error)]
pub enum JoinError {
    /// Indicates that no thread was available to be joined.
    #[error("no thread was available to be joined")]
    NoHandleFound,

    /// Indicates that the joined thread panicked with the given value.
    #[error("the joined thread panicked before exiting normally")]
    ThreadPanic(Box<dyn Any + Send + 'static>),
}
