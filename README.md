# `go-spawn`

`go-spawn` is a library that provides macros to spawn and join threads with minimal boilerplate. 
Threads can be spawned with either `go!` or `go_ref!` and can be joined with either `join!` or
`join_all!`.

## Installation

`go-spawn` is published on `https://crates.io`. To use in your Rust project, add the latest version
of `go-spawn` to your `Cargo.toml` file:

```toml
[dependencies]
go-spawn = "0.1.0"
``` 

## Examples.

```rust
use go_spawn::{go, join};
use std::sync::{
    atomic::{AtomicI64, Ordering},
    Arc,
};

let counter = Arc::new(AtomicI64::new(0));
let counter_cloned = counter.clone();

// Spawn a thread that captures values by move.
go! {
    for _ in 0..100 {
        counter_cloned.fetch_add(1, Ordering::SeqCst));
    }
}

// Join the most recent thread spawned by `go_spawn` that has not yet been joined.
assert!(join!().is_ok());
assert_eq!(counter.load(Ordering::SeqCst), 100);
```

```rust
use go_spawn::{go_ref, join};
use std::sync::{
    atomic::{AtomicI64, Ordering},
};

static COUNTER: AtomicI64 = AtomicI64::new(0);

for _ in 0..10 {
    // Spawn a thread that captures by reference.
    go_ref!(COUNTER.fetch_add(1, Ordering::SeqCst));
}

// Join all not-yet-joined threads spawned by `go_spawn`.
join_all!();
assert_eq!(counter.load(Ordering::SeqCst), 10);
```

## Documentation

Documentation can be found at `https://docs.rs/go-spawn`.